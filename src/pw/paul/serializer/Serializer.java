package pw.paul.serializer;

public interface Serializer {

  int write(byte[] dataPool, int address, byte value);

  int writeShort(byte[] dataPool, int address, short value);

  int writeInt(byte[] dataPool, int address, int value);

  int writeFloat(byte[] dataPool, int address, float value);

  int writeLong(byte[] dataPool, int address, long value);

  int writeDouble(byte[] dataPool, int address, double value);

  int writeString(byte[] dataPool, int address, String value);

  int writeBool(byte[] dataPool, int address, boolean value);

  byte readByte(byte[] dataPool, int address);

  short readShort(byte[] dataPool, int address);

  int readInt(byte[] dataPool, int address);

  float readFloat(byte[] dataPool, int address);

  long readLong(byte[] dataPool, int address);

  double readDouble(byte[] dataPool, int address);

  String readString(byte[] dataPool, int address);

  boolean readBool(byte[] dataPool, int address);

}
