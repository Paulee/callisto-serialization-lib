package pw.paul.serializer.impl;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import pw.paul.serializer.Serializer;

import static pw.paul.util.CallistoUtil.*;

public class SimpleSerializer implements Serializer {

  private final ByteOrder order;

  public SimpleSerializer() {
    this(ByteOrder.BIG_ENDIAN);
  }

  public SimpleSerializer(ByteOrder order) {
    this.order = order;
  }

  @Override
  public int write(byte[] dataPool, int address, byte value) {
    if (hasNoRoom(dataPool, address, value)) {
      throw new IllegalStateException("Data pool is to small!");
    }

    dataPool[address++] = value;

    return address;
  }

  @Override
  public int writeShort(byte[] dataPool, int address, short value) {
    if (hasNoRoom(dataPool, address, value)) {
      throw new IllegalStateException("Data pool is to small!");
    }

    for ( byte aByte : getAsBytes(value) ) {
      this.write(dataPool, address++, aByte);
    }

    return address;
  }

  @Override
  public int writeInt(byte[] dataPool, int address, int value) {
    if (hasNoRoom(dataPool, address, value)) {
      throw new IllegalStateException("Data pool is to small!");
    }

    for ( byte aByte : getAsBytes(value) ) {
      this.write(dataPool, address++, aByte);
    }

    return address;
  }

  @Override
  public int writeFloat(byte[] dataPool, int address, float value) {
    if (hasNoRoom(dataPool, address, value)) {
      throw new IllegalStateException("Data pool is to small!");
    }

    for ( byte aByte : getAsBytes(value) ) {
      this.write(dataPool, address++, aByte);
    }

    return address;
  }

  @Override
  public int writeDouble(byte[] dataPool, int address, double value) {
    if (hasNoRoom(dataPool, address, value)) {
      throw new IllegalStateException("Data pool is to small!");
    }

    for ( byte aByte : getAsBytes(value) ) {
      this.write(dataPool, address++, aByte);
    }

    return address;
  }

  @Override
  public int writeLong(byte[] dataPool, int address, long value) {
    if (hasNoRoom(dataPool, address, value)) {
      throw new IllegalStateException("Data pool is to small!");
    }

    for ( byte aByte : getAsBytes(value) ) {
      this.write(dataPool, address++, aByte);
    }

    return address;
  }

  @Override
  public int writeString(byte[] dataPool, int address, String value) {
    if (hasNoRoom(dataPool, address, value)) {
      throw new IllegalStateException("Data pool is to small!");
    }

    address = this.writeInt(dataPool, address, value.length());

    for ( byte aByte : value.getBytes() ) {
      this.write(dataPool, address++, aByte);
    }

    return address;
  }

  @Override
  public int writeBool(byte[] dataPool, int address, boolean value) {
    if (hasNoRoom(dataPool, address, value)) {
      throw new IllegalStateException("Data pool is to small!");
    }

    this.write(dataPool, address++, (byte) boolToInt(value));

    return address;
  }

  @Override
  public byte readByte(byte[] dataPool, int address) {
    return dataPool[address];
  }

  @Override
  public short readShort(byte[] dataPool, int address) {
    return ByteBuffer.wrap(dataPool, address, sizeof(Short.class)).order(this.order)
      .getShort();
  }

  @Override
  public int readInt(byte[] dataPool, int address) {
    return ByteBuffer.wrap(dataPool, address, sizeof(Integer.class)).order(this.order)
      .getInt();
  }

  @Override
  public float readFloat(byte[] dataPool, int address) {
    return ByteBuffer.wrap(dataPool, address, sizeof(Float.class)).order(this.order)
      .getFloat();
  }

  @Override
  public long readLong(byte[] dataPool, int address) {
    return ByteBuffer.wrap(dataPool, address, sizeof(Long.class)).order(this.order)
      .getLong();
  }

  @Override
  public double readDouble(byte[] dataPool, int address) {
    return ByteBuffer.wrap(dataPool, address, sizeof(Double.class)).order(this.order)
      .getDouble();
  }

  @Override
  public String readString(byte[] dataPool, int address) {

    int length = readInt(dataPool, address);

    return new String(
      ByteBuffer.wrap(Arrays.copyOfRange(dataPool, address + 4, length + 4), 0, length)
        .order(this.order).array());
  }

  @Override
  public boolean readBool(byte[] dataPool, int address) {
    return intToBool(this.readByte(dataPool, address));
  }
}
