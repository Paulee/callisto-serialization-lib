package pw.paul;

import pw.paul.serializer.Serializer;
import pw.paul.serializer.impl.SimpleSerializer;

public final class Controller {

  private static final Controller INSTANCE = new Controller();

  private Serializer serializer;

  private Controller() {
    this.serializer = new SimpleSerializer();
  }

  public Serializer getDefault() {
    return this.serializer;
  }

  public void setSerializer(Serializer serializer) {
    this.serializer = serializer;
  }

  public static Controller get() {
    return INSTANCE;
  }

}
