package pw.paul.util;

public final class CallistoUtil {

  private CallistoUtil() {}

  public static boolean intToBool(int n) {
    return n != 0;
  }

  public static int boolToInt(boolean bool) {
    return bool ? 1 : 0;
  }

  public static boolean hasNoRoom(byte[] dataPool, int address, Object obj) {
    return dataPool.length < address + sizeof(obj);
  }

  public static <T extends Number> boolean isBig(T value) {
    return value.getClass().equals(Long.class) || value.getClass().equals(Double.class);
  }

  public static <T extends Number> int translateToInt(T value) {
    return value.getClass().equals(Float.class) ?
      Float.floatToRawIntBits(value.floatValue()) : value.intValue();
  }

  public static <T extends Number> long translateToLong(T value) {
    return value.getClass().equals(Double.class) ?
      Double.doubleToRawLongBits(value.doubleValue()) : value.longValue();
  }

  public static <T extends Number> byte[] getAsBytes(T value) {
    byte[] bytes = new byte[sizeof(value)];

    for ( int i = 0; i < bytes.length; i++ ) {
      bytes[i] = (byte) ((isBig(value) ? translateToLong(value)
        : translateToInt(value)) >> ((bytes.length - 1 - i) * 8) & 0xFF);
    }

    return bytes;
  }

  public static int sizeof(Object obj) {
    return obj.getClass().equals(String.class) ? ((String) obj).getBytes().length + 4
      : sizeof(obj.getClass());
  }

  public static int sizeof(Class<?> clazz) {
    try {
      return clazz.equals(Boolean.class) ? 1
        : clazz.getField("BYTES").getInt(Integer.class);
    } catch (IllegalAccessException | NoSuchFieldException e) {
      System.err.println("Not supported Object!");
    }
    return -1;
  }

}
